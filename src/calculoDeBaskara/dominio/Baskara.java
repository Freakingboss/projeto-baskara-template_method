package calculoDeBaskara.dominio;

public class Baskara extends CalculoRaizesSegundoGrau{

	@Override
	public int calcularDelta(int a, int b, int c) {
		return Delta.calcularDelta(a, b, c);
	}

	@Override
	public Raizes calcularBaskara(int a, int b, int delta) {
		
		final double primeiraRaiz = calcularPrimeiraRaiz(a, b, delta);
		final double segundaRaiz = calcularSegundaRaiz(a, b, delta);
		
		return new Raizes(primeiraRaiz, segundaRaiz);
	}
	
	private double calcularPrimeiraRaiz(int a, int b, int delta) {
		return ((-b + Math.sqrt(delta))/(2 * a));
	}
	
	private double calcularSegundaRaiz(int a, int b, int delta) {
		return ((-b - Math.sqrt(delta))/(2 * a));
	}

}
