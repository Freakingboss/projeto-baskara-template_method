package calculoDeBaskara.dominio;

public class Delta{
	
	static int calcularDelta(int a, int b, int  c) {
		return (b * b) - 4 * a * c;
	}

}
