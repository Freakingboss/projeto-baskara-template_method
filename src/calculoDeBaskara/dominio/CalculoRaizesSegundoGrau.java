package calculoDeBaskara.dominio;

public abstract class CalculoRaizesSegundoGrau {
	
	public Raizes calcular(int a, int b, int c) {
		
		final int delta = calcularDelta(a, b, c);
		
		return calcularBaskara(a, b, delta);
	}
	
	public abstract int calcularDelta(int a, int b, int c);
	
	public abstract Raizes calcularBaskara(int a, int b, int delta);
	
}
