package calculoDeBaskara.dominio;

public class Raizes {
	private final double primeiraRaiz;
	private final double segundaRaiz;
	
	Raizes(double primeiraRaiz, double segundaRaiz){
		this.primeiraRaiz = primeiraRaiz;
		this.segundaRaiz = segundaRaiz;
	}

	public double getPrimeiraRaiz() {
		return primeiraRaiz;
	}

	public double getSegundaRaiz() {
		return segundaRaiz;
	}
	
	
}
