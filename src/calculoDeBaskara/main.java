package calculoDeBaskara;

import calculoDeBaskara.dominio.Baskara;
import calculoDeBaskara.dominio.CalculoRaizesSegundoGrau;
import calculoDeBaskara.dominio.Raizes;

public class main {

	public static void main(String[] args) {
		
		final CalculoRaizesSegundoGrau calculoRaizes = new Baskara();
		
		final Raizes raizes = calculoRaizes.calcular(2, 4, 2);
		
		System.out.println(raizes.getPrimeiraRaiz());
		System.out.println(raizes.getSegundaRaiz());

	}

}
